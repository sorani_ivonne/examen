package com.example.examensorani;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.examensorani.Api.Api;
import com.example.examensorani.Api.Servicio_Peticion;
import com.example.examensorani.ViewModels.Login;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    EditText correo, contrasena;
    Button login;
    public String APITOKEN = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = preferencias.getString("TOKEN", "");
        if(token != ""){
            Toast.makeText(MainActivity.this, "Bienvenido Nuevamente", Toast.LENGTH_LONG).show();
            startActivity(new Intent(MainActivity.this, PantallaProcesador.class));
        }

        correo = (EditText) findViewById(R.id.correo);
        contrasena = (EditText) findViewById(R.id.contrasena);
        login = (Button) findViewById(R.id.login);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!correo.getText().toString().isEmpty() && !contrasena.getText().toString().isEmpty()){
                    Servicio_Peticion service = Api.getApi(MainActivity.this).create(Servicio_Peticion.class);
                    Call<Login> loginCall =  service.getLoginPractica(correo.getText().toString(),contrasena.getText().toString());
                    loginCall.enqueue(new Callback<Login>() {
                        @Override
                        public void onResponse(Call<Login> call, Response<Login> response) {
                            Login peticion = response.body();
                            if(peticion.getEstado()){
                                APITOKEN = peticion.getToken();
                                guardarPreferencias();
                                Toast.makeText(MainActivity.this, "Bienvenido", Toast.LENGTH_LONG).show();
                                startActivity(new Intent(MainActivity.this, PantallaProcesador.class));
                            }else{
                                Toast.makeText(MainActivity.this, "Datos Incorrectos", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Login> call, Throwable t) {
                            Toast.makeText(MainActivity.this, "Error :(", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else{
                    Toast.makeText(MainActivity.this, "Llene campos please", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void guardarPreferencias(){
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = APITOKEN;
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putString("TOKEN", token);
        editor.commit();
    }
}

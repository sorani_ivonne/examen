package com.example.examensorani.ViewModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ClaseProcesador {
    @SerializedName("estado")
    @Expose
    private Boolean estado;
    @SerializedName("procesadores")
    @Expose
    private List<Procesadore> procesadores = null;

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public List<Procesadore> getProcesadores() {
        return procesadores;
    }

    public void setProcesadores(List<Procesadore> procesadores) {
        this.procesadores = procesadores;
    }
}

package com.example.examensorani.ViewModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DetalleProcesador {
    @SerializedName("estado")
    @Expose
    private Boolean estado;
    @SerializedName("procesador")
    @Expose
    private Procesadore procesador;

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public DetalleProcesador withEstado(Boolean estado) {
        this.estado = estado;
        return this;
    }

    public Procesadore getProcesador() {
        return procesador;
    }

    public void setProcesador(Procesadore procesador) {
        this.procesador = procesador;
    }

    public DetalleProcesador withProcesador(Procesadore procesador) {
        this.procesador = procesador;
        return this;
    }

}

package com.example.examensorani.ViewModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Procesadore {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("nombre")
    @Expose
    private String nombre;
    @SerializedName("nucleos")
    @Expose
    private String nucleos;
    @SerializedName("frecuencia")
    @Expose
    private String frecuencia;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Procesadore withId(Integer id) {
        this.id = id;
        return this;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Procesadore withNombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public String getNucleos() {
        return nucleos;
    }

    public void setNucleos(String nucleos) {
        this.nucleos = nucleos;
    }

    public Procesadore withNucleos(String nucleos) {
        this.nucleos = nucleos;
        return this;
    }

    public String getFrecuencia() {
        return frecuencia;
    }

    public void setFrecuencia(String frecuencia) {
        this.frecuencia = frecuencia;
    }

    public Procesadore withFrecuencia(String frecuencia) {
        this.frecuencia = frecuencia;
        return this;
    }
}

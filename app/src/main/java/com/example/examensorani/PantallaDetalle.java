package com.example.examensorani;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.example.examensorani.Api.Api;
import com.example.examensorani.Api.Servicio_Peticion;
import com.example.examensorani.ViewModels.DetalleProcesador;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PantallaDetalle extends AppCompatActivity {

    TextView id, pass;
    String parametro, cod;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_detalle);
        id = (TextView) findViewById(R.id.id);
        pass = (TextView) findViewById(R.id.pass);
        parametro = getIntent().getExtras().getString("parametro");
        cod = getIntent().getExtras().getString("cod");


        Servicio_Peticion service = Api.getApi(PantallaDetalle.this).create(Servicio_Peticion.class);
        Call<DetalleProcesador> loginCall = service.getDetallesProcesador(Integer.parseInt(cod));
        loginCall.enqueue(new Callback<DetalleProcesador>() {
            @Override
            public void onResponse(Call<DetalleProcesador> call, Response<DetalleProcesador> response) {
                DetalleProcesador peticion = response.body();
                if (peticion.getEstado()) {
                    id.setText("DETALLES DEL PROCESADOR");
                    pass.setText("Frecuencia: " + peticion.getProcesador().getFrecuencia() + "\n"
                            + "Procesador: " + peticion.getProcesador().getNombre() + "\n"
                            + "Núcleos: " + peticion.getProcesador().getNucleos() + "\n"
                            + "ID de procesador: " + peticion.getProcesador().getId() + "\n"
                    );
                } else {
                    Toast.makeText(PantallaDetalle.this, "Datos Incorrectos", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<DetalleProcesador> call, Throwable t) {
                Toast.makeText(PantallaDetalle.this, "Error :(", Toast.LENGTH_SHORT).show();
            }
        });
    }
}

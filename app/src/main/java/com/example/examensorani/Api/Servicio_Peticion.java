package com.example.examensorani.Api;

import com.example.examensorani.ViewModels.ClaseProcesador;
import com.example.examensorani.ViewModels.DetalleProcesador;
import com.example.examensorani.ViewModels.Login;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface Servicio_Peticion {
    @FormUrlEncoded
    @POST("api/loginSocial")
    Call<Login> getLoginPractica(@Field("username") String correo, @Field("password") String contrasenia);

    @FormUrlEncoded
    @POST("api/detalleProcesador")
    Call<DetalleProcesador> getDetallesProcesador(@Field("procesadorId") int id);

    @POST("api/informacionProcesadores")
    Call<ClaseProcesador> getProcesador();
}

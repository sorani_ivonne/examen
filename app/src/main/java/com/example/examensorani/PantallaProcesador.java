package com.example.examensorani;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.examensorani.Api.Api;
import com.example.examensorani.Api.Servicio_Peticion;
import com.example.examensorani.ViewModels.ClaseProcesador;
import com.example.examensorani.ViewModels.Procesadore;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PantallaProcesador extends AppCompatActivity {

    List<Procesadore> usuariosList;
    RecyclerView recyclerView;
    RecyclerAdapter recyclerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_procesador);

        usuariosList = new ArrayList<>();
        recyclerView = (RecyclerView)findViewById(R.id.recyclerview);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerAdapter = new RecyclerAdapter(getApplicationContext(),usuariosList);
        recyclerView.setAdapter(recyclerAdapter);

        Servicio_Peticion service = Api.getApi(PantallaProcesador.this).create(Servicio_Peticion.class);
        Call<ClaseProcesador> noticias =  service.getProcesador();
        noticias.enqueue(new Callback<ClaseProcesador>() {
            @Override
            public void onResponse(Call<ClaseProcesador> call, Response<ClaseProcesador> response) {

                ClaseProcesador user= response.body();

                if (user.getEstado()){
                    recyclerAdapter.SetUsuarios(user.getProcesadores());
                } else
                    Toast.makeText(PantallaProcesador.this, "Petición no procesada", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<ClaseProcesador> call, Throwable t) {
                Log.d("TAG","Response = "+t.toString());
                Toast.makeText(PantallaProcesador.this, "Error: " + t.getMessage(), Toast.LENGTH_LONG).show();
                t.printStackTrace();
            }
        });

    }
}
